/****************************************************************************
  
 Autor: Jonathan Finlay
 Fecha: 25/05/2009
 Email: jmfinlayp@gmail.com
     
Fuentes del generador de sintaxis "SynGen v0.2"

Archivo "mainwindow.ui.h"

Contiene las funciones que permiten configurar archivos de sintaxis para spss 11
en adelante usando CTABLES, genera un archivo que permite ejecutar una secuencia de 
comandos spss que:

Eliminan pesos y filtros previos
Genera tablas de frecuencia para todas las variables configuradas
Configuran factores de ponderacion adecuados
Configuran filtros adecuados
Y generan tablas cruzadas

Fue escrito en Qt 3.3 y portado a Qt 4.4 con qt3to4 para funcionar
en ambiente M$ Window$.

c:\Qt\2009.02\qt\bin\qt3to4.exe SynGen.pro
  
*****************************************************************************/
#include <qvalidator.h>
#include <qlineedit.h>
#include <qlistview.h>
#include <qmessagebox.h> 
#include <qfile.h>
#include <qfiledialog.h>

void MainWindow::init() //Inicializacion y validacion de datos
{
    lineEditNumVariables->setValidator( new QIntValidator( lineEditNumVariables ) );
    lineEditNumVariablesCruce->setValidator( new QIntValidator( lineEditNumVariablesCruce ) );
    lineEditPrimeraVariable->setValidator( new QIntValidator( lineEditPrimeraVariable ) );
    textEditFiltros->setText("");
    listViewIdentificacion->setSorting(1,  FALSE);
    listViewVariables -> setSorting( 1 , FALSE); //No ordenar las listView
}

void MainWindow::destroy( ){
    
}

// Tab page "Frecuencias"

void MainWindow::add_var_identificacion() // Crear nuevos items dentro de listViewIdentificacion
{
    if (lineEditIdentificacion->text() != "") {
	new QListViewItem(listViewIdentificacion, lineEditIdentificacion->text() );
	lineEditIdentificacion->setText("");
    }
}


void MainWindow::edit_var_identificacion() // Activa la edicion de items seleecionados en listViewIdentificacion
{    
    QListViewItem *item = listViewIdentificacion->selectedItem();
    if ( item != NULL ){ 
	item->setRenameEnabled ( 0, TRUE );
	item->startRename ( 0 );
    }
}


void MainWindow::delete_var_identificacion() // Elimina items seleecionados en listViewIdentificacion
{
    listViewIdentificacion->takeItem(listViewIdentificacion->selectedItem());		
}


void MainWindow::identificacion_frecuencias() // Verifica y advierte de la no existencia de variables de identificacion
{
    if ( checkBoxFrecuencias->isChecked() ){
	if ( listViewIdentificacion->childCount() == 0 ){
	    QMessageBox::information(this, "Informacion", "No se han definido variables de identificacion \ndel encuestado");
	}
    }
}

// Tab Page "Datos Basicos"

void MainWindow::add_var_basicos() // Crear nuevos items dentro de listViewVariables
{
    if (lineEditVariables->text() != "") {
	new QListViewItem(listViewVariables, lineEditVariables->text() );
	lineEditVariables->setText("");
	num_basicos();
    }
}


void MainWindow::edit_var_basicos() // Activa la edicion de items seleecionados en listViewVariables
{    
    QListViewItem *item = listViewVariables->selectedItem();
    if ( item != NULL ){ 
	item->setRenameEnabled ( 0, TRUE );
	item->startRename ( 0 );
    }
}


void MainWindow::delete_var_basicos()  // Elimina items seleecionados en listViewVariables
{
    listViewVariables->takeItem(listViewVariables->selectedItem());
    num_basicos();
}


void MainWindow::num_basicos() // Numero de variables de cruce
{
    lineEditNumVariablesCruce->setText(QString::number(listViewVariables->childCount() ));
}

// Tab Filtros

void MainWindow::FiltrosIgual() // Condiciones para los filtros, imprime el signo = 
{
    textEditFiltros->insert(" = ");
    textEditFiltros->setFocus( );
}


void MainWindow::FiltrosAnd() // Condiciones para los filtros, imprime el signo &
{
    textEditFiltros->insert(" & ");
    textEditFiltros->setFocus( );
}


void MainWindow::FiltrosOr() // Condiciones para los filtros, imprime el signo |
{
    textEditFiltros->insert(" | ");
    textEditFiltros->setFocus(  );
}


void MainWindow::AbrirParentesis()  // Condiciones para los filtros, imprime el signo ( 
{
    textEditFiltros->insert(" ( ");
    textEditFiltros->setFocus(  );
}


void MainWindow::CerrarParentesis()  // Condiciones para los filtros, imprime el signo ) 
{
    textEditFiltros->insert(" ) ");
    textEditFiltros->setFocus( );
}


void MainWindow::LimpiarFiltro() // Limpia caja de texto en el que se definen los filtros
{
    textEditFiltros->setText("");
    textEditFiltros->setFocus(  );
}

// Tab Cruces

void MainWindow::configurarCruces() // Activa-desactiva la tabla de cruce bidimencionales
{
    if ( checkBoxCruces->isChecked() ){
	tableCruces->setEnabled( TRUE );
    }
    else{
	tableCruces->setEnabled( FALSE );	
    }
    
}

// Generador

void MainWindow::GenerarSintaxis(){ // Funcion para generar la sintaxis, solo llama a las funciones para generar sintaxis
    QString file;
    file = "";    
    file.append( cabecera() ); 
    if ( lineEditPrimeraVariable->text().toInt() < lineEditNumVariables->text().toInt()){
	if ( checkBoxFrecuencias->isChecked() )
	    file.append( frecuencias() );
	if ( lineEditPonderacion->text() != "" && lineEditPonderacion->text() != "" )
	    file.append( factoresPonderacion() );
	if ( textEditFiltros->text() != "" )
	    file.append( filtros() );
	if (  lineEditNumVariables->text() != "" && lineEditNumVariablesCruce->text() != "0" && lineEditPrimeraVariable->text() != "" ) 
	    file.append( cTables() );
	if ( checkBoxCruces->isChecked() )
	    file.append( crucesBidimensionales() );
	
	if ( save( file ) ){
	    QMessageBox::warning( this, "Alerta", "El archivo de sintaxis fue generado existosamente.");
	}
	else {
	    QMessageBox::warning( this, "Alerta", "Error al generar sintaxis. \nNo se ha generado ningun archivo.");
	}
    }
    else{
	QMessageBox::warning( this, "Alerta", "No se hace nada:\nLa primera variable a cruzar debe ser menor al numero total de variables!");
    }
}

QString MainWindow::cabecera() // Imprime en una cadena la cabecera y los comando para dehabilitar filtros y pesos, inicializa el archivo
{
    QString cabecera = "* Generado con SynGen v0.2\n\n";
    cabecera += "FILTER OFF. \nUSE ALL. \nEXECUTE. \nWEIGHT \n   OFF.\n\n";
    return cabecera;
}

QString MainWindow::frecuencias() // Genera la sintaxis para todas las variables. 
{
    QString str = "";
    if ( listViewIdentificacion -> childCount() != 0 && lineEditNumVariables->text() != "" ){
	str = "FREQUENCIES\n   VARIABLES= ";
	str += generadorVariables();
	str += "\n/ORDER=  ANALYSIS .\n\n";
    }
    else{
	QMessageBox::warning( this, "Alerta", "No se pudo generar sintaxis para frecuencias.");
    }
    return str;
}

QString MainWindow::generadorVariables() // Genera una cadena con todas las variables definidas
{
    QString str ="";
    int max = lineEditNumVariables->text().toInt();
    QListViewItem *currentItem = listViewIdentificacion -> firstChild();
    str += currentItem ->  text(0).upper();
    while ( currentItem -> itemBelow() != NULL ){
	currentItem = currentItem -> itemBelow();
	str += " ";
	str += currentItem -> text(0).upper();
    }
    str += " ";
    for ( int i = 1; i<=max; ++i ){
	str += " v";
	str += QString::number(i);
    }
    
    return str;
}

QString MainWindow::factoresPonderacion() // Genera sintaxis para factores de ponderacion
{
    QString strFactores ="RECODE\n\t";
    QTable * tabla = tableFactores;
    
    strFactores += lineEditPonderacion->text().upper();
    strFactores += "\n";
    for ( int j = 0; j < 30; ++ j ){
	if ( tabla->text( j, 0 ) != QString::null && tabla->text( j, 1 ) != QString::null ){
	    strFactores += "\t\t( ";
	    strFactores += tabla->text( j, 0 ).upper();
	    strFactores += " = ";
	    strFactores += tabla->text( j, 1 ).upper();
	    strFactores += ")\n ";
	}
    }
    
    strFactores +="\tINTO ";
    strFactores += lineEditResultante->text().upper();
    strFactores +=". \n";
    strFactores +="EXECUTE. \n\n"; 
    strFactores +="WEIGHT \n"; 
    strFactores +="\tBY ";
    strFactores += lineEditResultante->text().upper();	
    strFactores +=".\n";
    return strFactores;
}

QString MainWindow::filtros() // Genera sintaxis para filtro
{
    QString strFiltros ="\nUSE ALL.\n";
    strFiltros += "COMPUTE filter_$=";
    strFiltros += textEditFiltros->text().upper();
    strFiltros += ".\n";
    strFiltros += "VARIABLE LABEL filter_$ \' ";
    strFiltros += textEditFiltros->text().upper();
    strFiltros += "\'. \n";
    strFiltros += "VALUE LABELS filter_$  0 \'Not Selected\' 1 \'Selected\'.  \n";
    strFiltros += "FORMAT filter_$ (f1.0). \nFILTER BY filter_$. \nEXECUTE .\n\n";
    return strFiltros;
}

QString MainWindow::cTables() // Genera toda la sintaxis para las tablas
{
    QString strTablas ="";
    QString strPrimeraCruce;
    QString strUltimaCruce;
    QString strRestoVarCruce = "";
    QString strC = " [C] ";
    QString strMas = "+";
    
    QListViewItem * listaTemp = listViewVariables -> firstChild();
    
    strPrimeraCruce = listaTemp->text(0).upper();
    strPrimeraCruce += " ";
    
    if ( listViewVariables -> childCount() > 1 ){
	
	listaTemp = listViewVariables -> lastItem();
	strUltimaCruce = listaTemp->text(0).upper();	
	strUltimaCruce = " ";
    }
    
    for ( int i = lineEditPrimeraVariable->text().toInt() ; i <= lineEditNumVariables->text().toInt() ; ++i ) {
	strTablas +="\n\nCTABLES\n";
	strTablas +="\t/VLABELS VARIABLES=";
	strTablas += strPrimeraCruce;
	strTablas += cadenaVariablesCruce( " ", "" ); 
	strTablas += strUltimaCruce;
	strTablas += "v";
	strTablas += QString::number(i);
	strTablas += " \n\t\tDISPLAY=DEFAULT\n";
	strTablas += "\t/TABLE ";
	strTablas += strPrimeraCruce;
	strTablas += strC;	
	
	if ( listViewVariables -> childCount() > 1 )
	    strTablas += strMas;
	
	strTablas += cadenaVariablesCruce( strC, strMas );
	strTablas += "BY ";
	strTablas += "v";
	strTablas += QString::number(i);
	strTablas += "\n\t\t[C][ROWPCT.COUNT  COMMA40.1, TOTALS[COLPCT.COUNT  COMMA40.1]]\n";
	strTablas += "\t/SLABELS VISIBLE=NO\n";
	strTablas += "\t/CATEGORIES VARIABLES=";
	strTablas += strPrimeraCruce;
	strTablas += "ORDER=A KEY=VALUE EMPTY=INCLUDE TOTAL=YES LABEL=\'";
	strTablas += lineEditEtiqueta->text(); 
	strTablas += "\'\n";
	strTablas += "\t\tPOSITION=BEFORE\n";
	
	if ( listViewVariables -> childCount() > 1 ){
	    strTablas += "\t/CATEGORIES VARIABLES=";
	    strTablas += cadenaVariablesCruce( " ", "" ); 
	    strTablas += "\n\t\tORDER=A KEY=VALUE EMPTY=EXCLUDE\n";
	}
	
	strTablas += "\t/CATEGORIES VARIABLES=";
	strTablas += "v";
	strTablas += QString::number(i);
	strTablas += ordenaTablas( "v"+QString::number(i) , i - lineEditPrimeraVariable->text().toInt() );
	strTablas += "\t\tPOSITION=AFTER  \n\t/TITLES  \n\t\tTITLE=\'  \'.\n";
    }
    return strTablas;
}

QString MainWindow::crucesBidimensionales() // Genera sintaxis para los cruces bidimensionales
{
    QString strCruces ="";
    QTable * tabla = tableCruces;
    for ( int j = 0; j < 30; ++j){
	if ( tabla -> text( j, 0 ) != "" && tabla -> text( j, 1 ) != ""){
	    strCruces += "\nCTABLES \n";
	    strCruces += "\t/VLABELS VARIABLES= ";
	    strCruces +=tabla -> text( j, 0 );
	    strCruces += " ";
	    strCruces +=tabla -> text( j, 1 );
	    strCruces += " ";
	    strCruces +="DISPLAY=DEFAULT \n";
	    strCruces +="\t/TABLE ";
	    strCruces +=tabla -> text( j, 0 );
	    strCruces +=" BY ";
	    strCruces +=tabla -> text( j, 1 );
	    strCruces +=" [TABLEPCT.COUNT COMMA40.1] \n";
	    strCruces +="\t/SLABELS VISIBLE=NO \n";
	    strCruces +="\t/CATEGORIES VARIABLES= ";
	    strCruces +=tabla -> text( j, 0 );
	    strCruces += " ";
	    strCruces +=tabla -> text( j, 1 );
	    strCruces += " ";
	    strCruces +="ORDER=A KEY=VALUE EMPTY=EXCLUDE.";
	}
    }
    return strCruces;
}

QString MainWindow::cadenaVariablesCruce (QString strC, QString strMas) // Iteracciona con cTables() entrega una cadena con las variables intermedias para los cruces ejm. v1 v2 v3 v4 v5 v6 by v8 aqui se genera v2 v3 v4 v5 v6 pues v1 y v 8 van en otros puntos dentro de la sintaxis de las tablas. 
{
    QListViewItem * listaTemp = listViewVariables -> firstChild();
    QString str;
    if ( listViewVariables -> childCount() > 1 ){
	
	while ( listaTemp -> itemBelow() != NULL ){
	    listaTemp = listaTemp -> itemBelow();
	    str += listaTemp->text(0);
	    str += strC;	
	    if ( listaTemp -> itemBelow() != NULL )
		str += strMas;
	}
    }    
    return str;
}

bool MainWindow::save(QString file) // Abre dialogo de guardar como
{
#ifndef QT_NO_FILEDIALOG
    QString fn = QFileDialog::getSaveFileName( QString::null, QString::null, this );
    if ( !fn.isEmpty() )
	return saveAs( fn , file);
    return FALSE;
#endif
}

bool MainWindow::saveAs( const QString& fileName , QString file) // recibe como parametros el nombre del archivo en el que guardara la sintaxis y la cadena con la sintaxis, guarda el archivo en el disco duro.
{
    QFile f( fileName );
    if ( !f.open( IO_WriteOnly ) ) {
	QMessageBox::warning(this,"I/O Error",
			     QString("No se pudo abrir el archivo.\nProbablemente otra aplicacion lo haya bloqueado\nO no tiene permisos sobre este archivo.")
			     +fileName);
	return FALSE;
    }
    QTextStream t(&f);
    t << file;
    f.close();
    return TRUE;
}


void MainWindow::ordenarTablas( )
{
    if ( ordenarCheckBox->isChecked() ){	
	// Verificar que esten definadas variables de indentificacion del encuestados y variables-preguntas
	int max = lineEditNumVariables->text().toInt();
	int inicio = lineEditPrimeraVariable ->text().toInt();
	int totalVars = max;
	QString str;
	
	tablaOrden->setEnabled( TRUE );
	
	if ( totalVars > 0 && inicio > 0 ){
	    // Generar datos de para la tabla
	    
	    QStringList orden, ordenPor;
	    orden << "Ascendente" << "Descendente";
	    ordenPor << "Valor" << "Variable";
	    
	    tablaOrden->setNumRows( max-inicio+1 );
	    
	    for ( int i = inicio; i<=max; ++i ){
		str = " v";
		str += QString::number(i);
		tablaOrden->setText( i - (inicio), 0 , str) ;
		tablaOrden->setItem( i - (inicio), 1, new QComboTableItem( tablaOrden, orden, FALSE ) );
		tablaOrden->setItem( i - (inicio), 2, new QComboTableItem( tablaOrden, ordenPor, FALSE ) );
	    }
	    
	}
	else{
	    QMessageBox::warning(this, "Error", "No existen variables definidas.\nNumero de variables de la encuesta = 0 \ny/o No se ha definido la primera variable de cruce." );
	    ordenarCheckBox->setChecked(FALSE);
	    tablaOrden->setEnabled( FALSE );
	}
    }
    else{
	tablaOrden->setEnabled( FALSE );	
    }
}

QString MainWindow::ordenaTablas( QString str, int j ){// Genera el codigo que permite ordenar los resultados de las tablas de distintas maneras.
    QString resul;
    if ( tablaOrden->isEnabled() ){
	if ( tablaOrden->text ( j ,1 ) == "Ascendente" && tablaOrden->text ( j ,2 ) == "Valor" ){
	    return " ORDER=A KEY=VALUE EMPTY=INCLUDE TOTAL=YES LABEL=\'Frecuencia\' \n"; 	   
	}
	else{
	    if( tablaOrden->text ( j ,1 ) == "Descendente" && tablaOrden->text ( j ,2 ) == "Valor" ){
		return " ORDER=D KEY=VALUE EMPTY=INCLUDE TOTAL=YES LABEL=\'Frecuencia\' \n"; 
	    }
	    else{
		if( tablaOrden->text ( j ,1 ) == "Ascendente" && tablaOrden->text ( j ,2 ) == "Variable" ){
		    return " ORDER=A KEY=COUNT("+str+") EMPTY=INCLUDE TOTAL=YES LABEL=\'Frecuencia\' \n"; 
		}
		else{
		    return " ORDER=D KEY=COUNT("+str+") EMPTY=INCLUDE TOTAL=YES LABEL=\'Frecuencia\' \n"; 
		}
	    }
	}
    }
    else{
	return " ORDER=A KEY=VALUE EMPTY=INCLUDE TOTAL=YES LABEL=\'Frecuencia\' \n";
    }
}

